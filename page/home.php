<?php

 ob_start();
 session_start();
 
/**************************************************************/
            # - Dichiarazione variabili d'ambiene - #
 $conn = require_once '../lib/function/connection.php';
 require_once 'configuration/variable.php';
 require_once '../lib/function/functions.php';
 $users = getUsers([]);
 
/************************************************************/
 

 
/******************* Avvio Controllo Sessione ***************/ 
 if(!$_SESSION['username']){header("Location: index.php");}
 else{
/******************* Fie Controllo Sessione ***************/      
     
 if($users):
    foreach ((array)$users as $userRow):
        if($userRow == null){exit;}
        /* Dichiarazione variabile username*/
        $username = $userRow['username'];
        # Start HTML
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $gdrConf['name_gdr']; ?></title>
    <link rel='stylesheet' href='../lib/nprogress/nprogress.css'/>
    <script src='../lib/nprogress/nprogress.js'></script>
    <script>
        NProgress.start();
    </script>
</head>
<body>
    
    
    <?php require_once '../themes/oscurity/header.inc.php'; ?>
   
    
    <!--<a href="lib/function/logout.php">logout</a>-->
    <!--  GOOGLE FONTS  -->
    <script>
        NProgress.configure({ easing: 'ease', speed: 2500 });
        NProgress.done();
    </script>
    <link href="https://fonts.googleapis.com/css?family=Bubbler+One" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>   
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/mainjs/home.min.js"></script>
    <script src="../themes/oscurity/js/home.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $gdrConf['link'].$gdrConf['attual_theme'].$gdrConf['css_dir']."home.min.css";?>">
</body>
</html>
<?php  
    endforeach;
    endif;
 }
    ob_end_flush(); 
?>