<?php
    /* File per la configurazione del gdr */
    $config = [
        # Set Default link with http://
        'link' => 'http://localhost/dragarian/', 
        # Set Name of GDR
        'name_gdr' => 'Dragonian',
        # Modificare per il value in modo da cambiare template
        'attual_theme' =>'themes/oscurity/',
        # Set css theme dir
        'css_dir' => 'css/',
        # Set js theme dir
        'js_dir' => 'js/',
        /****************** Start themes configuration ************/
        # USE on or off to ability or disability 
        # Set top-menu - left-menu - right-menu
        'top-menu'   => 'on',
        'left-menu'  => 'on',
        'right-menu' => 'on',
        
        /*********************************************************/
        
    ];
    
    return $config;
?>