<?php
    /* File per la configurazione dei dati riguardanti il database */
    
    $config = [
      'mysql_host' => 'localhost',
      'mysql_username' => 'root',
      'mysql_password' => '',
      'mysql_database' => 'dragonian'
    ];
    
    return $config;
?>