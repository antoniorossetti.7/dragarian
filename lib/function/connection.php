<?php
    #error_reporting(0);
    $conn = require 'configuration/config.php';
    #var_dump($conn);
    
    $mysqli = new mysqli(
                $conn['mysql_host'],$conn['mysql_username'],
                $conn['mysql_password'],$conn['mysql_database']
            );
    
    /* Gestione eventuali errori della connessione */
    if ($mysqli && $mysqli->connect_error){
        
        /*In fase di sviluppo uso qusto die*/
        die($mysqli->connect_error);
        
        /* Mentre in fase di produzione o per la post produzione 
         * uso questo  
         * die('problemca conttattando il server');
        */
    }
    /* Fine gestione eventuali errori della connessione */
?>
