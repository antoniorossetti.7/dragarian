<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dragarian</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Homepage style-->
    <link href="css/homepage.min.css" rel="stylesheet">
    <!--  GOOGLE FONTS  -->
    <link href="https://fonts.googleapis.com/css?family=Bubbler+One" rel="stylesheet">
    <link rel='stylesheet' href='lib/nprogress/nprogress.css'/>
    <script src='lib/nprogress/nprogress.js'></script>
    <script>
        NProgress.start();
    </script>
  </head>
  <body style="">
    
      <div class="container-fluid">
          
        <div class="row-fluid">
            
            <div class="video" id="sliding-left">
                
                <video autoplay  muted loop>
                    <source src="video/video.mp4" type="video/mp4">
                </video>
                
                
                
                <div class="col-sm-12">
                    
                    <center>
                        
                        <blockquote>
                            
                                <!-- «Quelli che ci amano non ci lasciano mai veramente. E puoi sempre trovarli. Qui dentro». -->
                                <h3>
                                    The ones that love us, never really leave us<br>|-|
                                    go on new world <i class="glyphicon glyphicon-th-large" id="open-world" data-toggle="tooltip"  title="Click Me"></i>|-|
                                </h3>
                            
                        </blockquote>
                        
                    </center>
                    
                </div>
                
            </div>
            <!-- Fine primo tempo -->
            <div class="second_history">
                <div class="cover-second"> 
                    
                </div>
                <div class="col-md-12 vertical-center" id="box-form">
                    <p class="copyright">&copy; Copyright 2017</p>
                    <?php
                        if ( isset($errMSG) ) {
                    ?>
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                    <form action="lib/function/accesso.php" method="post">
                        <div class="form-group">
                          <label for="username">Username</label>
                          <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                        </div>

                        <div class="form-group">
                          <label for="password">Password</label>
                          <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                            
                        <button type="submit" name="btn-login" class="btn btn-danger form-control">Login</button>
                            
                    </form>
                    <div class="link">
                        <a href="">Registrazione</a> | 
                        <a href="">Ambientazione</a> |
                        <a href="">Regolamento</a>  
                    </div>
                    
                </div>
                
            </div>
            
        </div>
          
    </div>
        <script>
          NProgress.configure({ easing: 'ease', speed: 2500 });
          NProgress.done();
        </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Include main js -->
    <script src="js/mainjs/index.min.js"></script>
  </body>
</html>